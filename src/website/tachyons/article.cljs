(ns website.tachyons.article)

(defn full-bleed-bg [{:keys [title content] :as optional}]
  [:article {:data-name "article-full-bleed-background"}
   [:div.cf.contain
    (when (:image optional)
      {:style
       {:background (str "url(" (:image optional) ")")
        :background-position "top right"
        :background-repeat "no-repeat" ;"left center"
        ;:background-size "cover"
        }})
    [:div.fl.pa3.pa4-ns.bg-white.black-70.measure.f6.f5-m.f4-l.measure-l
     [:header.bb.b--black-70.pv4
      [:h3.f2.fw7.ttu.tracked.lh-title.mt0.mb3.avenir title]
      (when (:sub-title optional)
        [:h4.f3.fw4.i.lh-title.mt0 (:sub-title optional)])]
     [:section.pt5.pb4.lh-copy
      content]]]])

(defn photo-essay [{:keys [title content] :as optional}]
  [:article.helvetica.pb5
   [:header.vh-100.bg-light-pink.dt.w-100
    [:div.dtc.v-mid.cover.ph3.ph4-m.ph5-l
     (when (:bakground-image optional)
       {:style
        {:background (str "url(" (:background-image optional) ")")
         :no-repeat "center right"
         :background-size "cover"}})
     [:h1.f2.f-subheadline-l.measure.lh-title.fw9 title]
     (when (:sub-title optional) [:h2.f6.fw6.black (:sub-title optional)])]]

   [:div.serif.ph3.ph4-m.ph5-l
    (when (:intro optional) [:p.lh-copy.f5.f3-m.f1-l.measure.center.pv4 (:intro optional)])
    [:div.f5.f3-m.lh-copy
     (when (:content optional)
       [:div.cf.dt-l.w-100.bt.b--black-10.pv4
        [:div.dtc-l.v-mid.mw6.pr3-l
         [:img.w-100 {:src (:image optional) :alt ""}]]
        [:div.dtc-l.v-mid.f6.f5-m.f4-l.measure-l
         [:p.measure.pv4-l.center content]]])
     (when (:images optional)
       [:div.cf
        [:div.fl.w-100.w-50-l.pr2-l.pb3
         (map (fn [{:keys [src] :as image}]
                [:img.db.w-100 {:src src :alt image}]) (:images optional))]])]]])

(defn essay [{:keys [title content] :as optional}]
  [:article.helvetica.pb5
   #_[:header.vh-100.bg-light-pink.dt.w-100
    [:div.dtc.v-mid.cover.ph3.ph4-m.ph5-l
     (when (:bakground-image optional)
       {:style
        {:background (str "url(" (:background-image optional) ")")
         :no-repeat "center right"
         :background-size "cover"}})
     [:h1.f2.f-subheadline-l.measure.lh-title.fw9 title]
     (when (:sub-title optional) [:h2.f6.fw6.black (:sub-title optional)])]]

   [:div.serif.ph3.ph4-m.ph5-l
    (when (:intro optional) [:p.lh-copy.f5.f3-m.f1-l.measure.center.pv4 (:intro optional)])
    [:div.f5.f3-m.lh-copy
     (when (:content optional)
       [:div.cf.dt-l.w-100.bt.b--black-10.pv4
        [:div.dtc-l.v-mid.mw6.pr3-l
         [:img.w-100 {:src (:image optional) :alt ""}]]
        [:div.dtc-l.v-mid.f6.f5-m.f4-l.measure-l
         content]])
     (when (:images optional)
       [:div.cf
        [:div.fl.w-100.w-50-l.pr2-l.pb3
         (map (fn [{:keys [src] :as image}]
                [:img.db.w-100 {:src src :alt image}]) (:images optional))]])]]])
