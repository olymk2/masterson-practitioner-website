(ns website.tachyons.cards)

(defn basic-text-card [{:keys [title content]}]
  [:article.center.mw5.mw6-ns.hidden.ba.mv4
   [:h1.f4.bg-near-black.white.mv0.pv2.ph3 title]
   [:div.pa3.bt [:p.f6.f5-ns.lh-copy.measure.mv0 content]]])


(defn text-card [{:keys [title content]}]
 [:article.center.mw5.mw6-ns.br3.hidden.ba.b--black-10.mv4
  [:h1.f4.bg-near-white.br3.br--top.black-60.mv0.pv2.ph3 title]
  [:div.pa3.bt.b--black-10 [:p.f6.f5-ns.lh-copy.measure content]]])


