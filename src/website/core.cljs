(ns website.core
  (:require [reagent.core :as reagent]
            [website.tachyons.headers :as header]
            ;[tachyon-reagent.tachyons.headers :as header]
            [website.tachyons.footers :as footer]
            [website.tachyons.navigation :as navigation]
            [website.tachyons.layout :as layout]
            [website.tachyons.definition-lists :as d-list]
            [website.tachyons.article :as article]
            [website.tachyons.articles :as a]))

(defn info []
  [:div
   [article/full-bleed-bg
    {:title "About me"
     :image "img/horse-01.jpg"
     :sub-title "Paula Marks - Masterson Practioner"
     :content [:<>
               [:p "Paula has always been around horses since she can remember and it has always remained her main
passion doing Pony Club and British Horse Society stages at school"]
               [:p "After purchasing an ex- racehorse that had kissing spine and serious stress issues a friend and trainer suggested looking at the Masterson method, she was so amazed by the results and changes
that it set her off on the journey to becoming a Certified Practitioner."]
               [:p "With all my horses I’ve seen a positive improvement not only performance but stress and arthritic issues and learning to connect with them better as a owner I compete in different disciplines and just
love seeing what they can achieve in what they enjoy doing."]]}]])

(defn contact []
  [:div
   [d-list/simple
    {:list [{:title "Contact" :text "Paula Marks"}
            {:title "E-Mail" :text
             [:a
              {:href (str "mailto:" "paula.marks83+masterson@gmail.com")} "paula.marks83@gmail.com"]}
            #_{:title "Facebook" :text ""}
            #_{:title "Instagram" :text ""}
            {:phone "07780990103"}
            #_{:title "address" :text "18 Raleigh Mead, South Molton, Devon, EX36 4BT"}]}]])

(defn booking-an-appointment []
  [:div
   [article/full-bleed-bg
    {:title "Book an appointment"
     :image "img/horse-01.jpg"
     :sub-title "Week and weekend appointments available"
     :content [:<>
               [contact]
               ;[:p "Appointments and fees Week and weekend appointments available"]
               [:p "Treatment fee £45.00"]
               [:p "A charge of 50p per mile will be added for anything over 15 Miles from South Molton."]
               [:p "Discount available when treating multiple horses at the same yard"]]}]])

(defn what-is-masterson []
  [:div
   [article/essay ;full-bleed-bg
    {;:title "What is masterson technique"
     :image "img/practioner-01.jpg"
     :sub-title "Paula Marks"
     :content
     [:<>
      [:p
       "The Masterson method is a unique, interactive method of equine bodywork that anyone can learn,
it helps to build trust with the horse, stress and stiffness is often resolved in a few sessions.
With the Masterson method you learn to recognise and use the responses of the horse to find and
relieve the tension."]
      [:p
       "In contrast to traditional massage it works with the horses nervous system and the horse actively
participates. The secret to releasing the muscle tension is by using a level of touch that bypasses the
horses flight or bracing response and allows them to release the tension."]
      [:p
       "Starting with a level of touch that barely touches the skin, we can activate the horses nervous
system to release amounts of tension using the simple theory of Search, Response, Stay & Release we
start to work the Masterson method."]]}]])

(defn page []
  (let [page (reagent/atom nil)]
    (fn []
      [:div
       [header/startup-hero {:title "Paula marks"
                             :sub-title "Masterson practioner"
                             :background-image "img/practioner-01.jpg"
                             :actions [{:title "Book appointment"
                                        :url "mailto:paula.marks83+masterson@gmail.com"}]
                             :links [{:title "Home" :url "#home" :callback #(reset! page :main)}
                                     {:title "About" :url "#about" :callback #(reset! page :info)}
                                     {:title "Book" :url "#book" :callback #(reset! page :appointment)}]}]
       (case @page
         :main [what-is-masterson]
         :appointment [booking-an-appointment]
         :info [info]
         [what-is-masterson])
       #_[footer/simple-large-type
          {:e-mail "paula.marks83+masterson@gmail.com"
           :content "info here"}]])))

(defn mount-root []
  (reagent/render [page]
                  (.getElementById js/document "app")))

(defn init! []
  (mount-root))

(init!)


